class InstallApplicationError(Exception):
    pass


class InstallStartupError(Exception):
    pass


class InstallCleanupError(Exception):
    pass


class InstallMiddlewareError(Exception):
    pass